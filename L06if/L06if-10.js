function S(x1,y1, x2,y2, x3, y3){
    var 
    AB = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)), 
    AC = Math.sqrt((x1-x3)*(x1-x3) + (y1-y3)*(y1-y3)), 
    CB = Math.sqrt((x2-x3)*(x2-x3) + (y2-y3)*(y2-y3));
    
    if (Math.abs(AB-AC)<0.00001 || 
        Math.abs(AC-CB)<0.00001 || 
        Math.abs(AB-CB)<0.00001)
        return "Равнобедренный";
    else
        return "Неравнобедренный";
}