// Задача - найти индекс элемента, наиболее близкого к среднему арифметическому

function f(a) {
    var sum = 0;
    
    for (var i=0; i < a.length; i++) { 
        sum = sum + a[i]; 
    }
    
    var avg = sum/a.length;
    var minR = Math.abs(a[0]-avg);
    var nearest = 0;
    
    for (i=1; i<a.length; i++) {
        if (Math.abs(a[i]-avg) < minR) {
            minR = Math.abs(a[i]-avg); 
            nearest = i;
        }
        
    }
    
    return nearest;
}

console.log(f([10, 11, 56, 24, 72, 18, 16, 25, 42, 80, 19, 33, 67]));
console.log(f([0,0,0,0,0,1]));
console.log(f([1,0,1,0]));